;;; Words for stack manipulations
	
	WORD p_dsp,'D[n]',fasm
	;; ( n -- a )
	;; Push the address of the n:th cell below n onto the stack
	pop rax
	shl rax,3
	add rax,rsp
	push rax
	next

	WORD p_depth,'DEPTH',fasm
	;; ( -- v )
	;; Push stack depth (before push)
	lea rax,[DS_TOP]
	sub rax,rsp
	shr rax,3
	push rax
	next

	WORD p_dup, 'DUP',fasm
	;; ( v -- v v )
	;; Duplicate top ov stack value.
	;; push qword [rsp] ??
	mov rax,qword [rsp]
	push rax
	next

	WORD p_2dup, '2DUP',fasm
	;; ( x1 x2 -- x1 x2 x1 x2 )
	;; Duplicate cell pair x1 x2.
	push qword [rsp+8]
	push qword [rsp+8]
	next

	WORD p_drop, 'DROP',fasm
	;; ( x -- )
	;; Remove x from the stack.
	add rsp,8
	next

	WORD p_2drop, '2DROP',fasm
	;; ( x1 x2 -- )
	;; Drop cell pair x1 x2 from the stack.
	add rsp,16
	next

	WORD p_over, 'OVER',fasm
	;; ( x1 x2 -- x1 x2 x1 )
	;; Place a copy of x1 on top of the stack. 
	push qword [rsp+8]
	next

	WORD p_2over, '2OVER',fasm
	;; ( x1 x2 x3 x4 -- x1 x2 x3 x4 x1 x2 )
	;; Copy cell pair x1 x2 to the top of the stack.
	push qword [rsp+24]
	push qword [rsp+24]
	next

	WORD p_swap, 'SWAP',fasm
	;; ( x1 x2 -- x2 x1 )
	;; Exchange the top two stack items.
	mov rax,qword [rsp]
	mov rbx,qword [rsp+8]
	mov qword [rsp+8],rax
	mov qword [rsp],rbx
	next

	WORD p_2swap, '2SWAP',fasm
	;; ( x1 x2 x3 x4 -- x3 x4 x1 x2 )
	;; Exchange the top two cell pairs.
	mov rax,qword [rsp]
	mov rbx,qword [rsp+16]
	mov qword [rsp], rbx
	mov qword [rsp+16],rax
	mov rax,qword [rsp+8]
	mov rbx,qword [rsp+24]
	mov qword [rsp+8], rbx
	mov qword [rsp+24],rax
	next

	WORD p_rot, 'ROT',fasm
	;; ( x1 x2 x3 -- x2 x3 x1 )
	;; Rotate the top three stack entries.
	mov rax,qword [rsp+16]
	mov rbx,qword [rsp+8]
	mov qword [rsp+16],rbx
	mov rbx,qword [rsp]
	mov qword [rsp+8],rbx
	mov qword [rsp],rax
	next

	;; ( xu xu-1 ... x0 u -- xu-1 ... x0 xu )
	;; Remove u. Rotate u+1 items on the top of the stack. An
	;; ambiguous condition exists if there are less than u+2 items
	;; on the stack before ROLL is executed.
	WORD p_roll, 'ROLL',fasm
	pop rcx
	shl rcx,3
	add rcx,rsp
	mov rax,[rcx+8]
p_roll_loop:
	cmp rcx,rsp
	je p_roll_eq
	mov rbx,[rcx]
	mov [rcx+8],rbx
	sub rcx,8
	jmp p_roll_loop
p_roll_eq:
	mov [rsp],rax
	next

	WORD p_nip, 'NIP',fasm
	;; ( x1 x2 -- x2 )
	;; Discard the second stack item. 
	pop rax
	mov qword [rsp],rax
	next

	WORD p_pick, 'PICK',fasm
	;; ( xu...x1 x0 u -- xu...x1 x0 xu )
	;; Remove u. Copy the xu to the top of the stack. An ambiguous
	;; condition exists if there are less than u+2 items on the
	;; stack before PICK is executed.
	pop rax
	shl rax,3		; 8 bytes per index
	push qword [rsp+rax]
	next

	WORD p_tuck, 'TUCK',fasm
	;; ( x1 x2 -- x2 x1 x2 )
	;; copy the top stack value into below second stack value.
	pop rax
	pop rbx
	push rax
	push rbx
	push rax
	next

;;; ========================================
;;; Return stack operations

	WORD p_gtR, '>R',fasm
	;; ( x -- ) ( R: -- x )
	;; Move x to the return stack.
	pop rax
	pushr rax
	next

	WORD p_Rgt, 'R>',fasm
	;; ( -- x ) ( R: x -- )
	;; Move x from the return stack to the data stack.
	popr rax
	push rax
	next

	WORD p_Rget, 'R@',fasm
	;; ( -- x ) ( R: x -- x )
	;; Copy x from the return stack to the data stack.
	push qword [rbp]
	next

	WORD p_rbp,'RSP',fasm
	;; Push the return stack pointer to the data stack
	push rbp
	next
	
	WORD p_rbpn,'R[n]',fasm
	;; ( n -- a )
	;; push the address of the n:th cell on the return stack
	mov rax,qword [rsp]
	shl rax,3
	add rax,rbp
	mov qword [rsp],rax
	next

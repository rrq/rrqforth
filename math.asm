;;; Words doing arithmetics
	
	WORD p_plus, '+',fasm
	;; ( n1 n2 -- n3 )
	;; n3 is the sum of n1 and n2
	pop rax
	add qword [rsp],rax
	next

	WORD p_minus, '-',fasm
	;; ( n1 n2 -- n3 )
	;; n3 is the result of subtracting n2 from n1
	pop rax
	sub qword [rsp], rax
	next

	WORD p_mult, '*',fasm
	;; ( n1 n2 -- n3 )
	;; multiply n1 * n2 to n3 ignoring overflow
	pop rax
	pop rbx
	imul rax,rbx
	push rax
	next

	WORD p_abs, 'ABS',fasm
	;; ( n -- u )
	;; u is the absolute value of n.
	cmp qword [rsp],0
	jge p_abs_end
	neg qword [rsp]
p_abs_end:
	next

	WORD p_max, 'MAX',fasm
	;; ( a b -- c )
	;; c is the least a and b
	pop rax
	cmp rax,qword [rsp]
	jle p_max_end
	mov qword [rsp],rax
p_max_end:
	next

	WORD p_min, 'MIN',fasm
	;; ( a b -- c )
	;; c is the least a and b
	pop rax
	cmp rax,qword [rsp]
	jge p_min_end
	mov qword [rsp],rax
p_min_end:
	next

	WORD p_negate, 'NEGATE',fasm
	;; ( n1 -- n2 )
	;; Negate n1, giving its arithmetic inverse n2. 
	xor rax,rax
	sub rax,qword [rsp]
	mov qword [rsp],rax
	next

	WORD p_divmod,'/MOD',fasm
	;; ( x y -- q r )
	;; divide signed x/y giving quotient q and remainder r
	pop rbx
	pop rax
	xor rdx,rdx
	idiv rbx
	push rax
	push rdx
	next

	WORD p_div,'/',fasm
	;; ( x y -- q )
	;; divide signed x/y giving quotient q and discard remainder
	pop rbx
	pop rax
	xor rdx,rdx
	idiv rbx
	push rax
	next


// logic.asm:	WORD p_0less, '0<',fasm

anchor:p_0less[]

=== Word: 0<

....
Data stack: ( v -- 0/-1 )
....

"0<" is a function word that replaces a value with -1 if the value is
less than 0, and 0 otherwise.

====
.Definition concept for 0<
****
( v -- 0/1 ) : 0= 0 SWAP < ;
****
====

See also <<p_swap,SWAP>> and <<p_lessthan,<>>.

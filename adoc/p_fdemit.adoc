// stdio.asm:	WORD p_fdemit,'FDEMIT'

anchor:p_fdemit[]

=== Word: FDEMIT

....
Data stack: ( c fd -- )
....

"FDEMIT" is a function word that puts the given character code to the
given file descriptor. The character is the least significant byte of
the data stack cell.


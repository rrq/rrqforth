// wordlists.asm:	WORD p_wordlist,'CURRENT-WORDLIST',dovariable

anchor:p_current_wordlist[]

=== Word: CURRENT-WORDLIST
....
Data stack: ( -- a )
....

"CURRENT-WORDLIST" is a variable word that points out the DFA of the
current word list word for <<p_find,FIND>> to use finding words. The
word list word content is as follows:

====
.word list word content
[caption='Layout {counter:layout}: ']
----
  8 TFA of latest word in the word list
  8 DFA of the/a subsequent word list to search through
----
====

Note that word lists are chained by "extension" and in general it ends
with the <<p_forth,FORTH>> word list. Initially the
<<p_system,SYSTEM>> word list is the only other word list.


// stack.asm:	WORD p_drop, 'DROP',fasm

anchor:p_drop[]

=== Word: DROP

....
Data stack: ( v -- )
....

"DROP" is a function word that discards the top stack cell.


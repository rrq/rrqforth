// stdio.asm:	WORD p_read_stream_line,'READ-STREAM-LINE'

anchor:p_read_stream_line[]

=== Word: READ-STREAM-LINE

....
Data stack: ( stream -- n )
....

"READ-STREAM-LINE" is a function word that gets the next line from the
given stream buffer into PAD and returns number of characters. If the
stream is backed by a file descriptor, the stream buffer is refilled
from there as needed, by a SYS_READ call when more characters are
needed.

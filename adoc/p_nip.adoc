// stack.asm:	WORD p_nip, 'NIP',fasm

anchor:p_nip[]

=== Word: NIP

....
Data stack: ( v1 v2 -- v2 )
....

"NIP" is a function word that discards the second of the top two cells
on the data stack.

====
.Definition concept for NIP
****
: NIP SWAP DROP ;
****
====

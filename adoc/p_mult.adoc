// math.asm:	WORD p_mult, '*',fasm

anchor:p_mult[]

=== Word: *

....
Data stack: ( v1 v2 -- v3 )
....

"*" is a function word that replaces a pair of values with the result
of multiplying them. To that end, the values are 64-bit signed
integers, and clipping the result to the least signifcant 64 bits.

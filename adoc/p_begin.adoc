// control.asm:	WORD p_begin,'BEGIN',fasm

anchor:p_begin[]

=== Word: BEGIN

....
Data stack: Compiling: ( -- a 0 )
....

"BEGIN" is an immediate function word that is used together with
<<p_ifbreak,IFBREAK>>, <<p_ifagain,IFAGAIN>> and <<p_end,END>> to
implement structured execution control. BEGIN simply places the
address for resolving branches back to this point during execution,
and then a 0 as a marker so as to allow for an unknown number of block
exit points.

====
.Usage example {counter:example}: 
----
: WTELL ( tfa -- ; Print word pname )
  24 + DUP 8 + SWAP @ TELL SP EMIT
;

: WORDS ( wordlist -- ; Print all words of word list )
  BEGIN
    @ DUP 0= IFBREAK
    DUP WTELL
  END
  DROP
  NL EMIT
;
----
====

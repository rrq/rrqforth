// stack.asm:	WORD p_definitions, 'DEFINITIONS',fasm

anchor:p_definitions[]

=== Word: DEFINITIONS

....
Data stack: ( wordlist -- )
....

"DEFINITIONS" is a function word that installs the given wordlist as
the <<p_current_wordlist,CURRENT-WORDLIST>> one.

====
.Definition concept for DEFINITIONS
****
: DEFINITIONS CURRENT-WORDLIST ! ;
****
====

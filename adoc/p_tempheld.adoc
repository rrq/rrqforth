// temp.asm:	WORD p_tempheld,'TEMPHELD',dovariable

anchor:p_tempheld[]

=== Word: TEMPHELD

....
Data stack: ( -- a )
....

"TEMPHELD" is a variable word that keeps the lowest offset of the
<<p__tempspace,TEMPSPACE>> space to reuse upon cycling. The space
below TEMPHELD is "held" in the sense of not being reused upon
cycling. An application may change the TEMPSPACE offset as needed to
dynamically preserve memory longer term.

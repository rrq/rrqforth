// memory.asm:	WORD p_erase, 'ERASE',fasm

anchor:p_erase[]

=== Word: ERASE

....
Data stack: ( a n -- )
....

"ERASE" is a function word that stores n NUL bytes at address a an up.

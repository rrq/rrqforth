// rrqforth.asm:	WORD p_dovariable,'doVARIABLE',dovariable

anchor:p_dovariable[]

=== Word: doVARIABLE

....
Data stack: ( -- a )
....

"doVARIABLE" is a variable word whose value is the implementation of
the execution semantics for "variables", which basically are markers
into the heap for some number block of memory.

The execution of a variable results in pushing its content address:
....
Resulting data stack: ( -- a )
....



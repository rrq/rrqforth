// contorl.asm:	WORD p_zero_branch,'0BRANCH',fasm

anchor:p_0branch[]

=== Word: 0BRANCH

....
Data stack: ( v -- )
....

"0BRANCH" is a function word that implements execution conditional by
means of optionally adding the subsequent branch offset, or not, to
the point of execution. If the value, v, is 0 then the branch offset
is added, and otherwise execution continues with the cell following
the branch offset in the definition.

Note that the branch offset is a byte count and each FORTH word of a
definition take up a cell of 8 bytes. The offset is relative to the
cell address immediately subsequent to the offset cell. In other
words, offset 0 is not branching anywhere and an offset of -16 would
make a tight loop back to the branch instruction itself. The latter
would pull data stack values until and including the first non-zero
value.

See also <<p_1branch,1BRANCH>>, <<p_branch,BRANCH>>, <<p_if,IF>>,
<<p_else,ELSE>>, <<p_ifbreak,IFBREAK>> and <<p_ifagain,IFAGAIN>>.

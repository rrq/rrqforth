// memory.asm:	WORD p_cfa2tfa, 'CFA>TFA',fasm

anchor:p_cfa2tfa[]

=== Word: CFA>TFA

....
Data stack: ( cfa -- tfa )
....

"CFA>TFA" is a function word that pushes word tfa of the given cfa.

====
.Definition concept for CFA>TFA
****
: CFA>TFA 14 - @ ;
****
====

// math.asm:	WORD p_plus, '+',fasm

anchor:p_plus[]

// note that asciidoc takes offence to a plain + in the title an
// therefore we here rely an a convenince macro
=== Word: {plus} 

....
Data stack: ( v1 v2 -- v3 )
....

"+" (plus) is a function word that replaces a pair of values with the
result of adding them. To that end, the values are 64-bit signed
integers.

// compile.asm:	WORD p_here,'HERE',dovariable

anchor:p_here[]

=== Word: HERE

....
Data stack: ( -- a )
....

"HERE" is a variable word that keeps the lowest address of the free
allocation space. It get updated by all words that allocate memory.


====
.allocate 1024 bytes on the heap
[caption='Usage example {counter:example} ']
****
1024 HEAP @ + HEAP !
****
====

See also <<p_allot,ALLOT>>.

// stack.asm:	WORD p_roll, 'ROLL',fasm

anchor:p_roll[]

=== Word: ROLL

....
Data stack: ( vu...v1 v0 u -- ...v1 v0 vu )
....

"ROLL" is a function word that "moves" the u:th data stack cell down
from top onto the data stack while discarding it. 0 indicates the top
cell; 1 indicates the second top cell making it the same as
<<p_swap,SWAP>>; 2 indicates the third top cell making it the same as
<<p_rot,ROT>>.





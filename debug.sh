#!/bin/bash

PFX='0x[0-9a-f]* rrqforth.asm:[0-9]*[ ]*'


grep "${PFX}do" rrqforth.map | sed 's/ .*//;s/^/break */' > gdbinit

NBR=$(wc -l < gdbinit)

I=1
while [ $I -lt $NBR ] ; do echo disable $I ; I=$((I+1)) ; done >> gdbinit

RET=( $(grep -w ret rrqforth.map | sed 's/ .*//') )
echo "break *${RET[0]}" >> gdbinit
#	;; rax = cfa of called word
#	;; rsi = cell* of next forth word
#	;; [$rsp] = from where the call was
cat <<EOF >> gdbinit
commands $((NBR+1))
print (void*) \$rsp
print (void*) \$rsi
print (char*)((*((void**)(*((void**)\$rsi)-16)))+32)
end
EOF

echo disable $NBR >> gdbinit
echo disable $((NBR+1)) >> gdbinit


DS="$(grep "${PFX}DS_TOP:" rrqforth.map | sed 's/ .*//')"
RS="$(grep "${PFX}RS_TOP:" rrqforth.map | sed 's/ .*//')"
cat <<EOF >> gdbinit
display *(void**)\$rsp@(((void*)$DS-\$rsp)/8)+1
display *(void**)\$rbp@(((void*)$RS-\$rbp)/8)+1
display (void*)\$rax
display (void*)\$rsi

define si
    stepi
    x/2i \$pc
    python gdb.execute("shell ./grepline.sh rrqforth.map " + str(gdb.parse_and_eval("\$pc")))
end

define map
    if \$argc == 1
        python gdb.execute("shell ./grepline.sh rrqforth.map \$arg0" + " 1 " )
    else
	python gdb.execute("shell ./grepline.sh rrqforth.map \$arg0" + " 1 " + "\$arg1" )
    end
end
EOF

exec gdb -x gdbinit rrqforth

#!/home/ralph/src/devuan/rrqforth/rrqforth
#
# List file descriptors

SYSTEM DEFINITIONS

### using execve
# Prepare empty environment on data stack and save pointer on return stack
0 0 D[n] >R

# Prepare the arg array on data stack and save pointer on return stack
0 " /proc/self/fd" DROP " -l" DROP " /bin/ls" DROP 0 D[n] >R

# The program to execute, arguments and environment
" /bin/ls" DROP  0 R[n] @  1 R[n] @  SYS_EXECVE

# Shouldn't come here, but in case...
" ERROR" TELL NL EMIT
1 EXIT

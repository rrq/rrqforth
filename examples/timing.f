#!/home/ralph/src/devuan/rrqforth/rrqforth
#
# This example adds the words:
# time0 = variable for holding a time stamp
# time = function to read of current time; seconds since epoch
# marktime = function that sets time0 from current time
# telltime = function that prints the difference between current time and time0
# longloop = test function for simple timing test

INPUT @ READ-WORD time0 CREATE DROP 0 , ( variable time0 )

: time ( -- s ; seconds since epoch )
  0 [ SYSTEM USE SYS_TIME TFA>CFA , ]
;

: marktime ( -- ; set time0 from current time )
  time time0 !
;

: telltime ( -- s ; read off time since time0 )
  time time0 @ - .
;

: longloop ( n -- )
  S" Takes " TELL marktime
  0 BEGIN
    1 + 2DUP > IFAGAIN
  END 2DROP
  telltime S"  seconds" TELL NL EMIT
;

1000000000 4 /
" Test run a tight loop of " TELL DUP . "  cycles" TELL NL EMIT
longloop

0 EXIT

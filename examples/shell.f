#!/home/ralph/src/devuan/rrqforth/rrqforth
#
# Run shell commands.

: VARIABLE ( "word" -- ; create a variable )
  INPUT @ READ-WORD CREATE DROP ;

: CONSTANT ( v "word" -- ; create a cell constant )
  INPUT @ READ-WORD CREATE TFA>CFA
  [ SYSTEM DEFINITIONS ] doVALUE [ FORTH DEFINITIONS ]
  SWAP ! , ;

: Z, ( char n -- ; Put string on the heap with terminating NUL )
  HERE @ SWAP DUP ALLOT STRNCPY 0 C, ;

VARIABLE SHELLZ " /bin/bash" Z,
VARIABLE -c " -c" Z,
VARIABLE ARGS SHELLZ , -c , 0 , 0 ,
VARIABLE ENV MAIN-ARGS DUP @ 8 * 8 + + ,
1 CONSTANT P_PID
4 CONSTANT WEXITED

: $ ( "rest of line" -- ; pass rest of line to a sub shell )
  [ SYSTEM DEFINITIONS ]
  INPUT @ READ-STREAM-LINE DROP
  SYS_FORK DUP 0=
  IF
    DROP PAD ARGS 16 + ! SHELLZ ARGS ENV SYS_EXECVE 0 EXIT
  THEN
  P_PID SWAP PAD WEXITED SYS_WAITID
  DUP IF S" **Wait error: " TELL . NL EMIT ELSE DROP THEN
  [ FORTH DEFINITIONS ]
;

: commands
  STDIN INPUT ! BEGIN S" $ " TELL $ AGAIN ;

" Type your commands...." TELL NL EMIT
commands

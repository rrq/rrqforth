#!/home/ralph/src/devuan/rrqforth/rrqforth

SYSTEM DEFINITIONS

: sleep ( seconds -- )
  0 SWAP 0 0           # set up 2 cell pairs for 2 "struct timespec"
  2 D[n] 0 D[n]        # Pointers to them ( *req *rem )
  SYS_NANOSLEEP DROP   # ignore return value
  2DROP 2DROP          # cleanup
;

: DOTS ( n -- ; sleep n seconds with a dot every second )
  BEGIN S" ." TELL 1 sleep 1 - DUP 0 > IFAGAIN SP EMIT END DROP ;

: DIEIF ( n -- ; die if n is 0 )
  0= IF S" ** not a number **" TELL NL EMIT 1 EXIT THEN ;

# Read a number
" Sleep seconds: " TELL STDIN READ-WORD NUMBER DIEIF
" sleeping " TELL DOTS " done" TELL NL EMIT

0 EXIT

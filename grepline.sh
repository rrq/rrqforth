#!/bin/bash
#
# This script implements some special commands that are set up for gdb
# in debug.sh

F=${1-rrqforth.map}

#echo "$*"

case "$3" in
    1)
	if [ -n "$4" ] ;then
	    if [ -z "${4%%-*}" ] ; then
		N=( -B "${4#-}" )
	    else
		N=( -A ${4} )
	    fi
	fi
	grep "${N[@]}" -E "$2" $F
	;;
    *)
	grep -E "(\\bWORD\\b|^$2) " $F | \
	    awk '$3 == "WORD" {x=$0;next} x!="" {print x; x=""}{ print;}'
esac

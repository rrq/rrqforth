;;; Logic words

	WORD p_and, 'AND',fasm
	;; ( x1 x2 -- x3 )
	;; x3 is the bit-by-bit logical "and" of x1 with x2.
	pop rax
	and qword [rsp], rax
	next

	WORD p_or, 'OR',fasm
	;; ( x1 x2 -- x3 )
	;; x3 is the bit-by-bit inclusive-or of x1 with x2. 
	pop rax
	or qword [rsp],rax
	next

	WORD p_xor, 'XOR',fasm
	;;  ( x1 x2 -- x3 )
	;; x3 is the bit-by-bit exclusive-or of x1 with x2.
	pop rax
	xor qword [rsp],rax
	next

	WORD p_not, 'NOT',fasm
	;; ( x -- v )
	;; v = 0 if x is non-zero and -1 otherwise
	not qword [rsp]
	next

	WORD p_false, 'FALSE',dovalue
	;; ( -- 0 )
	;; Push a false flag, 0.
	dq 0

	WORD p_true, 'TRUE',dovalue
	;; ( -- true )
	;; Return a true flag, -1. (non-zero)
	dq -1

pop_false_next:
	mov qword [rsp],0
	next

pop_true_next:
	mov qword [rsp],-1
	next

	WORD p_within, 'WITHIN',fasm
	;; ( n1 n2 n3 -- flag )
	;; Push true if n2 <= n1 and n1 < n3 and false otherwise.
	xor rcx,rcx
	pop rax
	pop rbx
	cmp qword [rsp],rbx
	jl pop_false_next
	cmp qword [rsp],rax
	jge pop_false_next
	jmp pop_true_next

	WORD p_0less, '0<',fasm
	;; ( n -- flag )
	;; flag is true (non-zero) if and only if n is less than zero.
	cmp qword [rsp],0
	jl pop_true_next
	jmp pop_false_next

	WORD p_0equal, '0=',fasm
	;; ( x -- flag )
	;; flag is true if x is equal to zero otherwise false.
	cmp qword [rsp],0
	je pop_true_next
	jmp pop_false_next
	
	WORD p_lessthan, '<',fasm
	;; ( n1 n2 -- flag )
	;; flag is true if and only if n1 is less than n2.
	pop rax
	cmp qword [rsp],rax
	jl pop_true_next
	jmp pop_false_next

	WORD p_lessequal, '<=',fasm
	;; ( n1 n2 -- flag )
	;; flag is true if and only if n1 is less than n2.
	pop rax
	cmp qword [rsp],rax
	jle pop_true_next
	jmp pop_false_next

	WORD p_equal, '=',fasm
	;; ( x1 x2 -- flag )
	;; flag is true if and only if x1 is bit-for-bit the same as
	;; x2.
	pop rax
	cmp qword [rsp],rax
	je pop_true_next
	jmp pop_false_next

	WORD p_unequal, '!=',fasm
	;; ( x1 x2 -- flag )
	;; flag is true if and only if x1 is bit-for-bit the same as
	;; x2.
	pop rax
	cmp qword [rsp],rax
	jne pop_true_next
	jmp pop_false_next
	
	WORD p_greaterthan, '>',fasm
	;; ( n1 n2 -- flag )
	;; flag is true if and only if n1 is greater than n2.
	pop rax
	cmp qword [rsp],rax
	jg pop_true_next
	jmp pop_false_next

	WORD p_greaterequal, '>=',fasm
	;; ( n1 n2 -- flag )
	;; flag is true if and only if n1 is greater than n2.
	pop rax
	cmp qword [rsp],rax
	jge pop_true_next
	jmp pop_false_next

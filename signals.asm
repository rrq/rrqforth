;;; Handle some signals

	WORD p_setup_signals,'[p_setup_signals]',dovariable
	;; Set up signal handling;
	mov rdi,11
	mov rsi,sigaction_data
	mov rdx,0
	sigsetsize = sigset.end - sigset.start
	mov r10,sigsetsize
	mov rax,13
	syscall
	ret

signal_handler_SEGV_message:
	STRING 10,'*** signal SEGV - restarting ***',10
	
signal_handler_SEGV:
	mov rdi,2
	mov rsi,signal_handler_SEGV_message + 8
	mov rdx,qword [signal_handler_SEGV_message]
	mov rax,1
	syscall
	jmp p_quit_DFA

signal_handler_SEGV_restorer:
	mov rax,15
	syscall

sigaction_data:
	dq signal_handler_SEGV ; void     (*sa_handler)(int);
	;dq 0 ; void     (*sa_sigaction)(int, siginfo_t *, void *);
        dq 0x44000000 ; unsigned long sa_flags
        dq 0 ;signal_handler_SEGV_restorer ; void     (*sa_restorer)(void);
sigset.start: ; sigset_t   sa_mask;
	rept 8 x {
          db 0 
	}
sigset.end:


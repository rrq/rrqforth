;;; https://chromium.googlesource.com/chromiumos/docs/+/master/constants/syscalls.md
;;;
;;;          id  rtn  0     1    2    3    4  5
;;; x86_64: rax, rax, rdi, rsi, rdx, r10, r8, r9

;;; %rax	System call	%rdi	%rsi	%rdx	%r10	%r8	%r9

;;; ==============================
;;; Helper macro for layout of syscall argument registers

macro SYSCALLARG register,count,nargs {
if count < nargs
	mov register, [rsp + ((nargs-count-1)*8)]
end if
}

;;; Code layout for system call with up to 6 arguments
macro SYSCALL id,name,fname,nargs {
	WORD name,fname,fasm
	pushr rsi
name#_asm:
	mov rax,qword id
	SYSCALLARG r9,5,nargs
	SYSCALLARG r8,4,nargs
	SYSCALLARG r10,3,nargs
	SYSCALLARG rdx,2,nargs
	SYSCALLARG rsi,1,nargs
	SYSCALLARG rdi,0,nargs
if nargs > 0
	add rsp,nargs*8
end if
	syscall
	push rax
	jmp exit
}

	SYSCALL 0,sys_read,'SYS_READ',3
	SYSCALL 1,sys_write,'SYS_WRITE',3
	SYSCALL 2,sys_open,'SYS_OPEN',3
	SYSCALL 3,sys_close,'SYS_CLOSE',1
	SYSCALL 4,sys_stat,'SYS_STAT',2
	SYSCALL 5,sys_fstat,'SYS_FSTAT',2				
	SYSCALL 6,sys_lstat,'SYS_LSTAT',2
	SYSCALL 7,sys_poll,'SYS_POLL',3
	SYSCALL 8,sys_lseek,'SYS_LSEEK',3
	SYSCALL 9,sys_mmap,'SYS_MMAP',6
	SYSCALL 10,sys_mprotect,'SYS_MPROTECT',3
	SYSCALL 11,sys_munmap,'SYS_MUNMAP',2
	SYSCALL 12,sys_brk,'SYS_BRK',1
	SYSCALL 13,sys_rt_sigaction,'SYS_RT_SIGACTION',4
	SYSCALL 14,sys_rt_sigprocmask,'SYS_RT_SIGPROCMASK',4
	SYSCALL 15,sys_rt_sigreturn,'SYS_RT_SIGRETURN',1
	SYSCALL 16,sys_ioctl,'SYS_IOCTL',3
	SYSCALL 17,sys_pread64,'SYS_PREAD64',4
	SYSCALL 18,sys_pwrite64,'SYS_PWRITE64',4
	SYSCALL 19,sys_readv,'SYS_READV',3
	SYSCALL 20,sys_writev,'SYS_WRITEV',3
	SYSCALL 21,sys_access,'SYS_ACCESS',2
	SYSCALL 22,sys_pipe,'SYS_PIPE',1
	SYSCALL 23,sys_select,'SYS_SELECT',5
	SYSCALL 24,sys_sched_yield,'SYS_SCHED_YIELD',0
	SYSCALL 25,sys_mremap,'SYS_MREMAP',5
	SYSCALL 26,sys_msync,'SYS_MSYNC',3
	SYSCALL 27,sys_mincore,'SYS_MINCORE',3
	SYSCALL 28,sys_madvise,'SYS_MADVISE',3
	SYSCALL 29,sys_shmget,'SYS_SHMGET',3
	SYSCALL 30,sys_shmat,'SYS_SHMAT',3
	SYSCALL 31,sys_shmctl,'SYS_SHMCTL',3
	SYSCALL 32,sys_dup,'SYS_DUP',1
	SYSCALL 33,sys_dup2,'SYS_DUP2',2
	SYSCALL 34,sys_pause,'SYS_PAUSE',0
	SYSCALL 35,sys_nanosleep,'SYS_NANOSLEEP',2
	SYSCALL 36,sys_getitimer,'SYS_GETITIMER',2
	SYSCALL 37,sys_alarm,'SYS_ALARM',1
	SYSCALL 38,sys_setitimer,'SYS_SETITIMER',3
	SYSCALL 39,sys_getpid,'SYS_GETPID',0
	SYSCALL 40,sys_sendfile,'SYS_SENDFILE',4
	SYSCALL 41,sys_socket,'SYS_SOCKET',3
	SYSCALL 42,sys_connect,'SYS_CONNECT',3
	SYSCALL 43,sys_accept,'SYS_ACCEPT',3
	SYSCALL 44,sys_sendto,'SYS_SENDTO',6
	SYSCALL 45,sys_recvfrom,'SYS_RECVFROM',6
	SYSCALL 46,sys_sendmsg,'SYS_SENDMSG',3			
	SYSCALL 47,sys_recvmsg,'SYS_RECVMSG',3			
	SYSCALL 48,sys_shutdown,'SYS_SHUTDOWN',2
	SYSCALL 49,sys_bind,'SYS_BIND',3
	SYSCALL 50,sys_listen,'SYS_LISTEN',2
	SYSCALL 51,sys_getsockname,'SYS_GETSOCKNAME',3
	SYSCALL 52,sys_getpeername,'SYS_GETPEERNAME',3
	SYSCALL 53,sys_socketpair,'SYS_SOCKETPAIR',4
	SYSCALL 54,sys_setsockopt,'SYS_SETSOCKOPT',5
	SYSCALL 55,sys_getsockopt,'SYS_GETSOCKOPT',5
	SYSCALL 56,sys_clone,'SYS_CLONE',5
	SYSCALL 57,sys_fork,'SYS_FORK',0
	SYSCALL 58,sys_vfork,'SYS_VFORK',0
	SYSCALL 59,sys_execve,'SYS_EXECVE',3
	SYSCALL 60,sys_exit,'SYS_EXIT',1
	SYSCALL 61,sys_wait4,'SYS_WAIT4',4
	SYSCALL 62,sys_kill,'SYS_KILL',2
	SYSCALL 63,sys_uname,'SYS_UNAME',1
	SYSCALL 64,sys_semget,'SYS_SEMGET',3
	SYSCALL 65,sys_semop,'SYS_SEMOP',3
	SYSCALL 66,sys_semctl,'SYS_SEMCTL',4
	SYSCALL 67,sys_shmdt,'SYS_SHMDT',1
	SYSCALL 68,sys_msgget,'SYS_MSGGET',2
	SYSCALL 69,sys_msgsnd,'SYS_MSGSND',4
	SYSCALL 70,sys_msgrcv,'SYS_MSGRCV',5
	SYSCALL 71,sys_msgctl,'SYS_MSGCTL',3
	SYSCALL 72,sys_fcntl,'SYS_FCNTL',3
	SYSCALL 73,sys_flock,'SYS_FLOCK',2
	SYSCALL 74,sys_fsync,'SYS_FSYNC',1
	SYSCALL 75,sys_fdatasync,'SYS_FDATASYNC',1
	SYSCALL 76,sys_truncate,'SYS_TRUNCATE',2
	SYSCALL 77,sys_ftruncate,'SYS_FTRUNCATE',2
	SYSCALL 78,sys_getdents,'SYS_GETDENTS',3
	SYSCALL 79,sys_getcwd,'SYS_GETCWD',2
	SYSCALL 80,sys_chdir,'SYS_CHDIR',1
	SYSCALL 81,sys_fchdir,'SYS_FCHDIR',1
	SYSCALL 82,sys_rename,'SYS_RENAME',2
	SYSCALL 83,sys_mkdir,'SYS_MKDIR',2
	SYSCALL 84,sys_rmdir,'SYS_RMDIR',1
	SYSCALL 85,sys_creat,'SYS_CREAT',2
	SYSCALL 86,sys_link,'SYS_LINK',2
	SYSCALL 87,sys_unlink,'SYS_UNLINK',1
	SYSCALL 88,sys_symlink,'SYS_SYMLINK',2
	SYSCALL 89,sys_readlink,'SYS_READLINK',3
	SYSCALL 90,sys_chmod,'SYS_CHMOD',2
	SYSCALL 91,sys_fchmod,'SYS_FCHMOD',2
	SYSCALL 92,sys_chown,'SYS_CHOWN',3
	SYSCALL 93,sys_fchown,'SYS_FCHOWN',3
	SYSCALL 94,sys_lchown,'SYS_LCHOWN',3
	SYSCALL 95,sys_umask,'SYS_UMASK',1
	SYSCALL 96,sys_gettimeofday,'SYS_GETTIMEOFDAY',2
	SYSCALL 97,sys_getrlimit,'SYS_GETRLIMIT',2
	SYSCALL 98,sys_getrusage,'SYS_GETRUSAGE',2
	SYSCALL 99,sys_sysinfo,'SYS_SYSINFO',1
	SYSCALL 100,sys_times,'SYS_TIMES',1
	SYSCALL 101,sys_ptrace,'SYS_PTRACE',4
	SYSCALL 102,sys_getuid,'SYS_GETUID',0
	SYSCALL 103,sys_syslog,'SYS_SYSLOG',3
	SYSCALL 104,sys_getgid,'SYS_GETGID',0
	SYSCALL 105,sys_setuid,'SYS_SETUID',1
	SYSCALL 106,sys_setgid,'SYS_SETGID',1
	SYSCALL 107,sys_geteuid,'SYS_GETEUID',0
	SYSCALL 108,sys_getegid,'SYS_GETEGID',0
	SYSCALL 109,sys_setpgid,'SYS_SETPGID',2
	SYSCALL 110,sys_getppid,'SYS_GETPPID',0
	SYSCALL 111,sys_getpgrp,'SYS_GETPGRP',0
	SYSCALL 112,sys_setsid,'SYS_SETSID',0
	SYSCALL 113,sys_setreuid,'SYS_SETREUID',2
	SYSCALL 114,sys_setregid,'SYS_SETREGID',2
	SYSCALL 115,sys_getgroups,'SYS_GETGROUPS',2
	SYSCALL 116,sys_setgroups,'SYS_SETGROUPS',2
	SYSCALL 117,sys_setresuid,'SYS_SETRESUID',3
	SYSCALL 118,sys_getresuid,'SYS_GETRESUID',3
	SYSCALL 119,sys_setresgid,'SYS_SETRESGID',3
	SYSCALL 120,sys_getresgid,'SYS_GETRESGID',3
	SYSCALL 121,sys_getpgid,'SYS_GETPGID',1
	SYSCALL 122,sys_setfsuid,'SYS_SETFSUID',1
	SYSCALL 123,sys_setfsgid,'SYS_SETFSGID',1
	SYSCALL 124,sys_getsid,'SYS_GETSID',1
	SYSCALL 125,sys_capget,'SYS_CAPGET',2
	SYSCALL 126,sys_capset,'SYS_CAPSET',2
	SYSCALL 127,sys_rt_sigpending,'SYS_RT_SIGPENDING',2
	SYSCALL 128,sys_rt_sigtimedwait,'SYS_RT_SIGTIMEDWAIT',4
	SYSCALL 129,sys_rt_sigqueueinfo,'SYS_RT_SIGQUEUEINFO',3
	SYSCALL 130,sys_rt_sigsuspend,'SYS_RT_SIGSUSPEND',2
	SYSCALL 131,sys_sigaltstack,'SYS_SIGALTSTACK',2
	SYSCALL 132,sys_utime,'SYS_UTIME',2
	SYSCALL 133,sys_mknod,'SYS_MKNOD',3
	;SYSCALL 134,sys_uselib,'SYS_USELIB',	NOT IMPLEMENTED
	SYSCALL 135,sys_personality,'SYS_PERSONALITY',1
	SYSCALL 136,sys_ustat,'SYS_USTAT',2
	SYSCALL 137,sys_statfs,'SYS_STATFS',2
	SYSCALL 138,sys_fstatfs,'SYS_FSTATFS',2
	SYSCALL 139,sys_sysfs,'SYS_SYSFS',3
	SYSCALL 140,sys_getpriority,'SYS_GETPRIORITY',2
	SYSCALL 141,sys_setpriority,'SYS_SETPRIORITY',3
	SYSCALL 142,sys_sched_setparam,'SYS_SCHED_SETPARAM',2
	SYSCALL 143,sys_sched_getparam,'SYS_SCHED_GETPARAM',2
	SYSCALL 144,sys_sched_setscheduler,'SYS_SCHED_SETSCHEDULER',3
	SYSCALL 145,sys_sched_getscheduler,'SYS_SCHED_GETSCHEDULER',1
	SYSCALL 146,sys_sched_get_priority_max,'SYS_SCHED_GET_PRIORITY_MAX',1
	SYSCALL 147,sys_sched_get_priority_min,'SYS_SCHED_GET_PRIORITY_MIN',1
	SYSCALL 148,sys_sched_rr_get_interval,'SYS_SCHED_RR_GET_INTERVAL',2
	SYSCALL 149,sys_mlock,'SYS_MLOCK',2
	SYSCALL 150,sys_munlock,'SYS_MUNLOCK',2
	SYSCALL 151,sys_mlockall,'SYS_MLOCKALL',1
	SYSCALL 152,sys_munlockall,'SYS_MUNLOCKALL',0
	SYSCALL 153,sys_vhangup,'SYS_VHANGUP',0
	SYSCALL 154,sys_modify_ldt,'SYS_MODIFY_LDT',3
	SYSCALL 155,sys_pivot_root,'SYS_PIVOT_ROOT',2
	SYSCALL 156,sys__sysctl,'SYS__SYSCTL',1
	SYSCALL 157,sys_prctl,'SYS_PRCTL',6
	SYSCALL 158,sys_arch_prctl,'SYS_ARCH_PRCTL',3
	SYSCALL 159,sys_adjtimex,'SYS_ADJTIMEX',1
	SYSCALL 160,sys_setrlimit,'SYS_SETRLIMIT',2
	SYSCALL 161,sys_chroot,'SYS_CHROOT',1
	SYSCALL 162,sys_sync,'SYS_SYNC',0
	SYSCALL 163,sys_acct,'SYS_ACCT',1
	SYSCALL 164,sys_settimeofday,'SYS_SETTIMEOFDAY',2
	SYSCALL 165,sys_mount,'SYS_MOUNT',5
	SYSCALL 166,sys_umount2,'SYS_UMOUNT2',2
	SYSCALL 167,sys_swapon,'SYS_SWAPON',2
	SYSCALL 168,sys_swapoff,'SYS_SWAPOFF',1
	SYSCALL 169,sys_reboot,'SYS_REBOOT',4
	SYSCALL 170,sys_sethostname,'SYS_SETHOSTNAME',2
	SYSCALL 171,sys_setdomainname,'SYS_SETDOMAINNAME',2
	SYSCALL 172,sys_iopl,'SYS_IOPL',2
	SYSCALL 173,sys_ioperm,'SYS_IOPERM',3
	;SYSCALL 174,sys_create_module,'SYS_CREATE_MODULE' REMOVED IN Linux 2.6
	SYSCALL 175,sys_init_module,'SYS_INIT_MODULE',3
	SYSCALL 176,sys_delete_module,'SYS_DELETE_MODULE',2
	;SYSCALL 177,sys_get_kernel_syms, REMOVED IN Linux 2.6
	;SYSCALL 178,sys_query_module,'SYS_QUERY_MODULE', REMOVED IN Linux 2.6
	SYSCALL 179,sys_quotactl,'SYS_QUOTACTL',4
	;SYSCALL 180,sys_nfsservctl,'SYS_NFSSERVCTL',	NOT IMPLEMENTED
	;SYSCALL 181,sys_getpmsg,'SYS_GETPMSG',	NOT IMPLEMENTED
	;SYSCALL 182,sys_putpmsg,'SYS_PUTPMSG',	NOT IMPLEMENTED
	;SYSCALL 183,sys_afs_syscall,'SYS_AFS_SYSCALL',	NOT IMPLEMENTED
	;SYSCALL 184,sys_tuxcall,'SYS_TUXCALL',	NOT IMPLEMENTED
	;SYSCALL 185,sys_security,'SYS_SECURITY',	NOT IMPLEMENTED
	SYSCALL 186,sys_gettid,'SYS_GETTID',0
	SYSCALL 187,sys_readahead,'SYS_READAHEAD',3
	SYSCALL 188,sys_setxattr,'SYS_SETXATTR',5
	SYSCALL 189,sys_lsetxattr,'SYS_LSETXATTR',5
	SYSCALL 190,sys_fsetxattr,'SYS_FSETXATTR',5
	SYSCALL 191,sys_getxattr,'SYS_GETXATTR',4
	SYSCALL 192,sys_lgetxattr,'SYS_LGETXATTR',4
	SYSCALL 193,sys_fgetxattr,'SYS_FGETXATTR',4
	SYSCALL 194,sys_listxattr,'SYS_LISTXATTR',3
	SYSCALL 195,sys_llistxattr,'SYS_LLISTXATTR',4
	SYSCALL 196,sys_flistxattr,'SYS_FLISTXATTR',3
	SYSCALL 197,sys_removexattr,'SYS_REMOVEXATTR',2
	SYSCALL 198,sys_lremovexattr,'SYS_LREMOVEXATTR',2
	SYSCALL 199,sys_fremovexattr,'SYS_FREMOVEXATTR',2
	SYSCALL 200,sys_tkill,'SYS_TKILL',2
	SYSCALL 201,sys_time,'SYS_TIME',1
	SYSCALL 202,sys_futex,'SYS_FUTEX',6
	SYSCALL 203,sys_sched_setaffinity,'SYS_SCHED_SETAFFINITY',3
	SYSCALL 204,sys_sched_getaffinity,'SYS_SCHED_GETAFFINITY',3
	;SYSCALL 205,sys_set_thread_area, NOT IMPLEMENTED. Use arch_prctl
	SYSCALL 206,sys_io_setup,'SYS_IO_SETUP',2
	SYSCALL 207,sys_io_destroy,'SYS_IO_DESTROY',2
	SYSCALL 208,sys_io_getevents,'SYS_IO_GETEVENTS',4
	SYSCALL 209,sys_io_submit,'SYS_IO_SUBMIT',3
	SYSCALL 210,sys_io_cancel,'SYS_IO_CANCEL',3
	;SYSCALL 211,sys_get_thread_area, NOT IMPLEMENTED. Use arch_prctl
	SYSCALL 212,sys_lookup_dcookie,'SYS_LOOKUP_DCOOKIE',3
	SYSCALL 213,sys_epoll_create,'SYS_EPOLL_CREATE',1
	;SYSCALL 214,sys_epoll_ctl_old,'SYS_EPOLL_CTL_OLD', NOT IMPLEMENTED
	;SYSCALL 215,sys_epoll_wait_old,'SYS_EPOLL_WAIT_OLD', NOT IMPLEMENTED
	SYSCALL 216,sys_remap_file_pages,'SYS_REMAP_FILE_PAGES',5
	SYSCALL 217,sys_getdents64,'SYS_GETDENTS64',3
	SYSCALL 218,sys_set_tid_address,'SYS_SET_TID_ADDRESS',1
	SYSCALL 219,sys_restart_syscall,'SYS_RESTART_SYSCALL',0
	SYSCALL 220,sys_semtimedop,'SYS_SEMTIMEDOP',4
	SYSCALL 221,sys_fadvise64,'SYS_FADVISE64',4
	SYSCALL 222,sys_timer_create,'SYS_TIMER_CREATE',3
	SYSCALL 223,sys_timer_settime,'SYS_TIMER_SETTIME',4
	SYSCALL 224,sys_timer_gettime,'SYS_TIMER_GETTIME',2
	SYSCALL 225,sys_timer_getoverrun,'SYS_TIMER_GETOVERRUN',1
	SYSCALL 226,sys_timer_delete,'SYS_TIMER_DELETE',1
	SYSCALL 227,sys_clock_settime,'SYS_CLOCK_SETTIME',2
	SYSCALL 228,sys_clock_gettime,'SYS_CLOCK_GETTIME',2
	SYSCALL 229,sys_clock_getres,'SYS_CLOCK_GETRES',2
	SYSCALL 230,sys_clock_nanosleep,'SYS_CLOCK_NANOSLEEP',4
	SYSCALL 231,sys_exit_group,'SYS_EXIT_GROUP',1
	SYSCALL 232,sys_epoll_wait,'SYS_EPOLL_WAIT',4
	SYSCALL 233,sys_epoll_ctl,'SYS_EPOLL_CTL',4
	SYSCALL 234,sys_tgkill,'SYS_TGKILL',3
	SYSCALL 235,sys_utimes,'SYS_UTIMES',2
	;SYSCALL 236,sys_vserver,'SYS_VSERVER',	NOT IMPLEMENTED
	SYSCALL 237,sys_mbind,'SYS_MBIND',6
	SYSCALL 238,sys_set_mempolicy,'SYS_SET_MEMPOLICY',3
	SYSCALL 239,sys_get_mempolicy,'SYS_GET_MEMPOLICY',5
	SYSCALL 240,sys_mq_open,'SYS_MQ_OPEN',4
	SYSCALL 241,sys_mq_unlink,'SYS_MQ_UNLINK',1
	SYSCALL 242,sys_mq_timedsend,'SYS_MQ_TIMEDSEND',5
	SYSCALL 243,sys_mq_timedreceive,'SYS_MQ_TIMEDRECEIVE',5
	SYSCALL 244,sys_mq_notify,'SYS_MQ_NOTIFY',2
	SYSCALL 245,sys_mq_getsetattr,'SYS_MQ_GETSETATTR',3
	SYSCALL 246,sys_kexec_load,'SYS_KEXEC_LOAD',4
	SYSCALL 247,sys_waitid,'SYS_WAITID',4
	SYSCALL 248,sys_add_key,'SYS_ADD_KEY',4
	SYSCALL 249,sys_request_key,'SYS_REQUEST_KEY',4
	SYSCALL 250,sys_keyctl,'SYS_KEYCTL',5
	SYSCALL 251,sys_ioprio_set,'SYS_IOPRIO_SET',3
	SYSCALL 252,sys_ioprio_get,'SYS_IOPRIO_GET',2
	SYSCALL 253,sys_inotify_init,'SYS_INOTIFY_INIT',0
	SYSCALL 254,sys_inotify_add_watch,'SYS_INOTIFY_ADD_WATCH',3
	SYSCALL 255,sys_inotify_rm_watch,'SYS_INOTIFY_RM_WATCH',2
	SYSCALL 256,sys_migrate_pages,'SYS_MIGRATE_PAGES',4
	SYSCALL 257,sys_openat,'SYS_OPENAT',4
	SYSCALL 258,sys_mkdirat,'SYS_MKDIRAT',3
	SYSCALL 259,sys_mknodat,'SYS_MKNODAT',4
	SYSCALL 260,sys_fchownat,'SYS_FCHOWNAT',5
	SYSCALL 261,sys_futimesat,'SYS_FUTIMESAT',3
	SYSCALL 262,sys_newfstatat,'SYS_NEWFSTATAT',4
	SYSCALL 263,sys_unlinkat,'SYS_UNLINKAT',3
	SYSCALL 264,sys_renameat,'SYS_RENAMEAT',4
	SYSCALL 265,sys_linkat,'SYS_LINKAT',5
	SYSCALL 266,sys_symlinkat,'SYS_SYMLINKAT',3
	SYSCALL 267,sys_readlinkat,'SYS_READLINKAT',4
	SYSCALL 268,sys_fchmodat,'SYS_FCHMODAT',3
	SYSCALL 269,sys_faccessat,'SYS_FACCESSAT',3
	SYSCALL 270,sys_pselect6,'SYS_PSELECT6',6
	SYSCALL 271,sys_ppoll,'SYS_PPOLL',5
	SYSCALL 272,sys_unshare,'SYS_UNSHARE',1
	SYSCALL 273,sys_set_robust_list,'SYS_SET_ROBUST_LIST',2
	SYSCALL 274,sys_get_robust_list,'SYS_GET_ROBUST_LIST',3
	SYSCALL 275,sys_splice,'SYS_SPLICE',6
	SYSCALL 276,sys_tee,'SYS_TEE',4
	SYSCALL 277,sys_sync_file_range,'SYS_SYNC_FILE_RANGE',4
	SYSCALL 278,sys_vmsplice,'SYS_VMSPLICE',4
	SYSCALL 279,sys_move_pages,'SYS_MOVE_PAGES',6
	SYSCALL 280,sys_utimensat,'SYS_UTIMENSAT',4
	SYSCALL 281,sys_epoll_pwait,'SYS_EPOLL_PWAIT',6
	SYSCALL 282,sys_signalfd,'SYS_SIGNALFD',3
	SYSCALL 283,sys_timerfd_create,'SYS_TIMERFD_CREATE',2
	SYSCALL 284,sys_eventfd,'SYS_EVENTFD',1
	SYSCALL 285,sys_fallocate,'SYS_FALLOCATE',4
	SYSCALL 286,sys_timerfd_settime,'SYS_TIMERFD_SETTIME',4
	SYSCALL 287,sys_timerfd_gettime,'SYS_TIMERFD_GETTIME',2
	SYSCALL 288,sys_accept4,'SYS_ACCEPT4',4
	SYSCALL 289,sys_signalfd4,'SYS_SIGNALFD4',4
	SYSCALL 290,sys_eventfd2,'SYS_EVENTFD2',2
	SYSCALL 291,sys_epoll_create1,'SYS_EPOLL_CREATE1',1
	SYSCALL 292,sys_dup3,'SYS_DUP3',3
	SYSCALL 293,sys_pipe2,'SYS_PIPE2',2
	SYSCALL 294,sys_inotify_init1,'SYS_INOTIFY_INIT1',1
	SYSCALL 295,sys_preadv,'SYS_PREADV',5
	SYSCALL 296,sys_pwritev,'SYS_PWRITEV',5
	SYSCALL 297,sys_rt_tgsigqueueinfo,'SYS_RT_TGSIGQUEUEINFO',4
	SYSCALL 298,sys_perf_event_open,'SYS_PERF_EVENT_OPEN',5
	SYSCALL 299,sys_recvmmsg,'SYS_RECVMMSG',5
	SYSCALL 300,sys_fanotify_init,'SYS_FANOTIFY_INIT',2
	SYSCALL 301,sys_fanotify_mark,'SYS_FANOTIFY_MARK',5	
	SYSCALL 302,sys_prlimit64,'SYS_PRLIMIT64',4
	SYSCALL 303,sys_name_to_handle_at,'SYS_NAME_TO_HANDLE_AT',5
	SYSCALL 304,sys_open_by_handle_at,'SYS_OPEN_BY_HANDLE_AT',5
	SYSCALL 305,sys_clock_adjtime,'SYS_CLOCK_ADJTIME',2
	SYSCALL 306,sys_syncfs,'SYS_SYNCFS',1
	SYSCALL 307,sys_sendmmsg,'SYS_SENDMMSG',4
	SYSCALL 308,sys_setns,'SYS_SETNS',2
	SYSCALL 309,sys_getcpu,'SYS_GETCPU',3
	SYSCALL 310,sys_process_vm_readv,'SYS_PROCESS_VM_READV',6
	SYSCALL 311,sys_process_vm_writev,'SYS_PROCESS_VM_WRITEV',6
	SYSCALL 312,sys_kcmp,'SYS_KCMP',5
	SYSCALL 313,sys_finit_module,'SYS_FINIT_MODULE',3
	SYSCALL 314,sys_sched_setattr,'SYS_SCHED_SETATTR',3
	SYSCALL 315,sys_sched_getattr,'SYS_SCHED_GETATTR',4
	SYSCALL 316,sys_renameat2,'SYS_RENAMEAT2',5
	SYSCALL 317,sys_seccomp,'SYS_SECCOMP',3
	SYSCALL 318,sys_getrandom,'SYS_GETRANDOM',3
	SYSCALL 319,sys_memfd_create,'SYS_MEMFD_CREATE',2
	SYSCALL 320,sys_kexec_file_load,'SYS_KEXEC_FILE_LOAD',5
	SYSCALL 321,sys_bpf,'SYS_BPF',3
	SYSCALL 322,sys_stub_execveat,'SYS_STUB_EXECVEAT',5
	SYSCALL 323,sys_userfaultfd,'SYS_USERFAULTFD',1
	SYSCALL 324,sys_membarrier,'SYS_MEMBARRIER',2
	SYSCALL 325,sys_mlock2,'SYS_MLOCK2',3
	SYSCALL 326,sys_copy_file_range,'SYS_COPY_FILE_RANGE',6
	SYSCALL 327,sys_preadv2,'SYS_PREADV2',6
	SYSCALL 328,sys_pwritev2,'SYS_PWRITEV2',6
	SYSCALL 329,sys_pkey_mprotect,'SYS_PKEY_MPROTECT',0
	SYSCALL 330,sys_pkey_alloc,'SYS_PKEY_ALLOC',0
	SYSCALL 331,sys_pkey_free,'SYS_PKEY_FREE',0
	SYSCALL 332,sys_statx,'SYS_STATX',0
	SYSCALL 333,sys_io_pgetevents,'SYS_IO_PGETEVENTS',0
	SYSCALL 334,sys_rseq,'SYS_RSEQ',0
	SYSCALL 335,sys_pkey_mprotect1,'SYS_PKEY_MPROTECT1',0

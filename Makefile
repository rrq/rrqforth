INCS := $(shell grep ^include rrqforth.asm | tr -d "'" | sed 's/include//' )

# compile into 3 things: binary, symbolic information and debug support
rrqforth: rrqforth.asm $(INCS)
	fasm $< -s $@.fas
	 ./fas2txt.lsp $@.fas > $@.map
	chmod a+x $@

clean:
	rm -f rrqforth{,.fas,.map}

ADOCS := $(shell echo adoc/*.adoc )
DOCS = reference.adoc wordindex.adoc separator.adoc $(ADOCS)

reference.html: $(DOCS)
	asciidoc -bhtml ${@:.html=.adoc} > $@

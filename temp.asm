;;; Managing a memory area for temporary "objects".
;;;
;;; Object space is allocated cyclically, starting subsequent to prior
;;; allocation and cycling back when requested object size exceeds
;;; the space edge. The "inital" part, which starts as empty, is
;;; considered "held" space that is not reused when cycling back, and
;;; apart from allowing for this, the space management is left open.


	WORD p_objectspace,'TEMPSPACE',dovariable
	;; Holds size and address of the object space.
	dq 1073741824 ; [0] Total object space size (request size)
	dq 0 ; [8] Object space base address.

	WORD p_tempused,'TEMPUSED',dovariable
	dq 0 ; [16] Currently used.

	WORD p_tempheld,'TEMPHELD',dovariable
	;; ( -- a )
	;; Marks the barrier for "held space"
	dq 0 ; [24] Currently held.

	WORD p_temp,'TEMP',fasm
	;; ( size -- addr )
	;; Allocate an object of given size
	pushr rsi
	cmp qword [p_objectspace_DFA+8],0
	jg p_objecthole_initialized
	;; initialize object space
	push qword [p_objectspace_DFA]
	DOFORTH p_malloc
	pop qword [p_objectspace_DFA+8]
p_objecthole_initialized:
	mov rax,qword [p_tempheld_DFA]
	cmp rax,qword [p_tempused_DFA]
	jl p_objecthole_recycle
	mov rax,qword [rsp]
	add rax,qword [p_tempused_DFA]
	cmp rax,qword [p_objectspace_DFA]
	jl p_objecthole_from_tail
	mov rax,qword [p_tempheld_DFA] ; cycling back to here
p_objecthole_recycle:
	mov qword [p_tempused_DFA],rax
	add rax,qword [rsp]
p_objecthole_from_tail:
	;; rax = new usage count
	mov rbx,qword [p_tempused_DFA]
	add rbx,qword [p_objectspace_DFA+8]
	mov qword [rsp],rbx
	mov qword [p_tempused_DFA],rax
	popr rsi
	next

	WORD p_str2temp,'STR>TEMP',fasm
	;; ( char* n -- char* n )
	;; Capture a given [n:char*] string as a new temp object with
	;; leading size cell.
	pushr rsi
	mov rax,qword [rsp]
	add rax,8
	push rax
	DOFORTH p_temp		; ( -- char* n  addr )
	pop rax
	pop rbx
	mov qword[rax],rbx
	add rax,8
	pop rcx
	push rax
	push rbx
	push rcx
	push rax
	push rbx		; ( -- addr n char* addr n )
	DOFORTH p_strncpy
	popr rsi
	next
